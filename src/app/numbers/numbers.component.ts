import {Component,} from '@angular/core';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})
export class NumbersComponent {
  numbers: number[] = [];
  password: any;
  constructor() {
    for (let i = 0; i < 5; i++) {
      this.numbers.push(this.getRandomNumb(5, 36));
      this.numbers.sort(function (a, b) {
        return a - b;
      });
    }
  }
  getRandomNumb(min: number, max: number) {
    return Math.floor(Math.random() * (max - min) + min);
  }
  onButtonClick() {
    for (let i = 0; i < 5; i++) {
      this.numbers.push(this.getRandomNumb(5, 36));
      this.numbers.sort(function (a, b) {
        return a - b;
      });
      this.numbers = this.numbers.filter(function(elem,index,self){
        return index === self.indexOf(elem);
      })
    }

  }

}
